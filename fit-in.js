function scale_images(selector)
{
    //console.log(jQuery('.nspImageWrapper img'));
    jQuery(selector).each(function() {
        var maxWidth = jQuery(this).parent().width();
        var maxHeight = jQuery(this).parent().height();
        var ratio = 0;  // Used for aspect ratio
        var width = jQuery(this).width();    // Current image width
        var height = jQuery(this).height();  // Current image height
        var native_size = getNativeSize(jQuery(this));

        console.log(jQuery(this));

        if(width > height)
        {
            ratio = maxWidth / width;   // get ratio for scaling image
        } else {
            ratio = maxHeight / height; // get ratio for scaling image
        }

        var calculated_width = width * ratio;
        if(calculated_width > native_size.width)
        {
            calculated_width = native_size.width;
        }
        jQuery(this).css("width", calculated_width); // Set new width

        var calculated_height = width * ratio;
        if(calculated_height > native_size.height)
        {
            calculated_height = native_size.height;
        }
        jQuery(this).css("height", calculated_height);  // Scale height based on ratio

        height = calculated_height;    // Reset height to match scaled image
        width = calculated_width;    // Reset width to match scaled image

        // horizontal align
        var margin_vertical = (maxHeight - height) / 2;

        if(margin_vertical > 0)
        {
            jQuery(this).css('margin-top', margin_vertical);
            jQuery(this).css('margin-bottom', margin_vertical);
        }

        // vertical align
        var margin_horizontal = (maxWidth - width) / 2;

        if(margin_horizontal > 0)
        {
            jQuery(this).css('margin-left', margin_horizontal);
            jQuery(this).css('margin-right', margin_horizontal);
        }
    });
}
/**
 * Gets native image size
 * @param img jQuery object
 * @returns {Object}
 */
function getNativeSize(img) {
    var oWidth = img.width();
    var oHeight = img.height();

    img.css('width', 'auto');
    img.css('height', 'auto');

    var rVal = new Object();
    rVal.width = img.width();
    rVal.height = img.height();

    //alert ('The image size is '+rVal.width+'x'+rVal.height);

    img.css('width', oWidth);
    img.css('height', oHeight);

    return rVal;
}


jQuery(document).ready(function(){
    //jQuery('.hikashop_main_carousel_div > div').jsCarousel();
});

jQuery(window).load(function() {
    // scaling
    var selector = 'img.fit-in';
    scale_images(selector);
    jQuery(selector).change(function(){
        scale_images(selector);
    })
});